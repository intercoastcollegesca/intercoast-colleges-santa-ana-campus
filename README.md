The central mission of InterCoast Colleges is to provide associates degrees and certificate programs for careers in allied health, business, and skilled trade industries and prepare students to meet employer expectations for entry level employment.


Address: 1720 E Garry Ave, Suite 103, Santa Ana, CA 92705, USA

Phone: 714-712-7900

Website: https://www.intercoast.edu
